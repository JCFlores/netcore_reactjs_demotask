import React, { Component, Fragment } from 'react';
import { Router, Route, Link } from 'react-router-dom';
import AppHeader from './components/AppHeader';
import Home from './components/Home';
import AppFooter from './components/AppFooter';
import LoginPage from './components/Login';
import { PrivateRoute } from './components/PrivateRoute';
import { authenticationService } from './_services/authentication.service';
import {history}  from  './constants'

class App extends Component {
  
    state = {
        currentUser: null
    };

  componentDidMount() {
    authenticationService.currentUser.subscribe(x => this.setState({ currentUser: x }));
}

logout() {
    authenticationService.logout();
    history.push('/login');
}

  render() {
    return <Fragment>
       <Router history={history}>
      <AppHeader />
      <PrivateRoute exact path="/" component={Home} />
       <Route path="/login" component={LoginPage} />     
      <AppFooter />
      </Router>
    </Fragment>;
  }
}

export default App;
