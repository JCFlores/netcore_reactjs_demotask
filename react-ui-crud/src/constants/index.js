import { createBrowserHistory } from 'history';

export const USERS_API_URL = 'https://localhost:5001/api/users';
export const USERS_API_Authenticate = 'https://localhost:5001/api/users/authenticate';

export const history = createBrowserHistory();