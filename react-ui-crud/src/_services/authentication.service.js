import { BehaviorSubject } from 'rxjs';
import { USERS_API_Authenticate } from '../constants';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('token')));

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
};

async function login(username, password) {

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return await fetch(`${USERS_API_Authenticate}`, requestOptions)
        .then(respose => {
            return respose.json();
        })
        .then(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('token', JSON.stringify(user.token));           
            currentUserSubject.next(user);
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    currentUserSubject.next(null);
}