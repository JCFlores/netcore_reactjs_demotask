import React from 'react';
import { Button, Form, FormGroup, Input, Label, Container, Row, Col } from 'reactstrap';
import { authenticationService } from '../_services/authentication.service';


class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        // redirect to home if already logged in
        if (authenticationService.currentUserValue) {
            this.props.history.push('/');
        }
    }

    state = {
        id: 0,
        username: '',
        password: '',
        token: ''
    }

    componentDidMount() {
        if (this.props.userCred) {
            const { id, username, token, } = this.props.userCred
            this.setState({ id, username, token });
        }
    }

    apiLogin = e => {
        console.log('state', this.state)
        e.preventDefault();
        authenticationService.login(this.state.username, this.state.password).then(data => {
            if (authenticationService.currentUserValue) {
                this.props.history.push('/');
            }
        });
    }

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    render() {
        return (
            <Container style={{ paddingTop: "100px" }}>
                <div className="alert alert-info">
                    Username: test<br />
                Password: test
            </div>

                <Row>
                    <Col md={{ span: 3, offset: 3 }}>
                        <h2>Login</h2>
                        <Form onSubmit={this.apiLogin}>
                            <FormGroup>
                                <Label for="username">Username</Label>
                                <Input name="username" type="text" onChange={this.onChange} value={this.state.username === '' ? '' : this.state.username} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Password</Label>
                                <Input name="password" type="password" onChange={this.onChange} value={this.state.password === '' ? '' : this.state.password} />
                            </FormGroup>
                            <Button type="submit" className="btn btn-primary">Login</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default LoginPage;