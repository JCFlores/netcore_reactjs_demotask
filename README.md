# Kambda ReactASPNET Demo Task
- Add basic users authentication
- Add JSON Web token header to the api endpoints
- Modify the user service to use a database (feel free to use the one you feel fits better)

Please submit your changes in a repository fork and let us know if it is ready sending an email to info@kambda.com and ccing vbalbas@kambda.com

Estimated Effort: 4 hours max

Expected Delivery: No later than 3 days after receiving the demo task at 5 pm or earlier.
