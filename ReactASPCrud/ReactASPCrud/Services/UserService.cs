using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReactASPCrud.AppConext;
using ReactASPCrud.Helper;
using ReactASPCrud.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ReactASPCrud.Services{

    public interface IUserService
    {
        LoginResponse Authenticate(LoginRequest model);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User Create(User user);
        void Update(int id, User user);
        void Delete(int id);
    }

    public class UserService : IUserService
    {

        private readonly AppSettings _appSettings;
        private AppDbContext _appDbContext;       

        public  UserService(AppDbContext appDbContext, IOptions<AppSettings> appSettings)
        {
            this._appDbContext = appDbContext;
            _appSettings = appSettings.Value;
        }

        public async Task<List<User>> GetAll()
        {
            var users = await _appDbContext.User.ToListAsync().ConfigureAwait(true);
            return users;
        }

        public User GetById(int id)
        {
            return _appDbContext.User.First(user => user.Id == id);
        }

        public  User Create(User user)
        {
            user.Password = "test";
            _appDbContext.User.Add(user);
            _appDbContext.SaveChanges();
 
            return user;
        }

        public void Update(int id, User user)
        {
            User found = _appDbContext.User.Where(n => n.Id == id).FirstOrDefault();
            found.Name = user.Name;
            found.Email = user.Email;
            found.Document = user.Document;
            found.Phone = user.Phone;

            _appDbContext.SaveChangesAsync();
        }

        public void Delete(int id)
        {
            var user = _appDbContext.User.SingleOrDefault(x => x.Id == id);
            _appDbContext.User.Remove(user);
            _appDbContext.SaveChangesAsync();
        }

        public LoginResponse Authenticate(LoginRequest model)
        {

            var user = _appDbContext.User.SingleOrDefault(x => x.Username == model.Username && x.Password == model.Password);
            if (user == null) return null;

            var token = generateJwtToken(user);
            return new LoginResponse(user, token);
        }

        IEnumerable<User> IUserService.GetAll()
        {
            return _appDbContext.User;
        }

        private string generateJwtToken(User user)
        {
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
