﻿using Microsoft.EntityFrameworkCore;
using ReactASPCrud.Models;

namespace ReactASPCrud.AppConext
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User[] {
              new User{Id=1, Password="test",Name="admin", Username="admin"},               
            });
        }
    }
}
