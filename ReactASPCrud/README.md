# Kambda ReactASPNET Demo Task

## Getting started
 - Configure DB SQL connection in appsetting.json
 - Execute 'Update-Database' Migration command. It will insert default admin user credential {username=admin, password=test}


## Verify DB status.
 -  run application and verify  https://localhost:5001/health response, if DB connection is Ok, it will return 'Healthy'

